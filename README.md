# PasteBin
## installation
### DreamTeam Squad - Paweł Wołoszyn, Sebastian Rulik, Dariusz Nowak, Rafał Jelonek, Jakub Binda

Projekt PasteBin umożliwia użytkownikom zapisywanie oraz udostępnianie napisanego kodu innym użtykownikom.

##Wykorzystane technologie

Projekt został zbudowany na Frameworku AngularJs, dzięki czemu wdrożono architekturę MVC oraz two-way-binding.

Dodatkowo wykorzystano następujące technologie oraz narzędzia.

 1. Gulp - jako task manager.
 2. Bower - jako package manager
 3. Json-server - mockowanie bazy oraz obsługa CRUD'a
 3. Bootstrap - framework css'owy
 4. Własny serwis do tłumaczeń - wszystkie tłumaczenia są w JSON

## Instalacja
install dependencies
```
npm install - w głównym folderze
```
install bourbon
```
bourbon install --force --path="src/scss/provider/bourbon"
```
create config file
```
gulp setup
```
build
```
gulp
```

## Przykładowy config
```
angular.module('app').constant('CONFIG', {
    apiUrl: 'http://localhost:3000',
    requestTimeout: '30000',
    localstorePrefix: 'us.',
    defaultLang: 'pl_PL',
});
```
## Przykładowa struktura modułu
Bazowa struktura

 - Controllers - kontrolery do widoku
 - Routing - ustawienie rutingu oraz podpinanie kontrolera, czy template
 - Templates - widoki danego modułu
 - Index.js - tworzenie nowego modułu angularowego

Dodatkowo występują też:

 - Filters - własne filtry
 - Services - dodatkowe serwisy

##Resources
Przykład
```
angular.module('app').factory('pastebinRepositories', [
    'CONFIG',
    '$resource',
    function(
        CONFIG,
        $resource
    ) {
        var resource = $resource('', {},
            {
                getCode: {
                    method: 'GET',
                    url: CONFIG.apiUrl + '/code/:id'
                },
                addCode : {
                  method: 'POST',
                  url: CONFIG.apiUrl + '/code'
                }
            }
        );

        return resource;
    }
```
## Odpalenie servera json-server
Należy utworzyć plik db.json
```
json-server --watch db.json
```
